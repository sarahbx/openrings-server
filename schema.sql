CREATE TABLE `ringlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ringKey` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` bigint(20) DEFAULT NULL,
  `owner` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`(255)),
  KEY `version` (`version`(255)),
  KEY `region` (`region`(255)),
  KEY `ringKey` (`ringKey`(255)),
  KEY `age` (`age`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
