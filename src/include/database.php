<?php
// database.php
// Database Object
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

class Database {
    private $address = "";
    private $username = "";
    private $password = "";
    private $dbname = "";
    private $config_loaded = false;

    protected $conn = null;

    public function __construct($address, $username, $password, $dbname) {
        if ($address && $username && $password && $dbname) {
            $this->address = $address;
            $this->username = $username;
            $this->password = $password;
            $this->dbname = $dbname;
            $this->config_loaded = true;
        }
        else {
            return null;
        }
    }

    function __destruct() {
        $this->close();
    }

    public function connect()
    {
        if ($this->config_loaded) {
            $this->conn = mysqli_connect($this->address, $this->username, $this->password) or mysqli_error($this->conn);
            mysqli_select_db($this->conn, $this->dbname) or mysqli_error($this->conn);
        }
        else {
            error_log("Database::connect() called without config loaded");
        }
    }

    public function close() {
        if ($this->conn) {
            mysqli_close($this->conn);
            $this->conn = null;
        }
    }

    public function escape($value) {
        $this->connect();
        $output = mysqli_real_escape_string($this->conn, $value);
        $this->close();
        return $output;
    }

    public function query($query) {
        $this->connect();
        $result_object = mysqli_query($this->conn, $query);
        if ($result_object === false or $result_object === true or $result_object === null) {
            if ($result_object === false) {
                error_log("database.php: QUERY ERROR:".$db_query."\n".mysqli_error($this->conn));
            }
            return $result_object;
        }
        else {
            $result_array = array();
            while ($temp_array = mysqli_fetch_array($result_object)) {
              $result_array[] = $temp_array;
            }
            return $result_array;
        }
        $this->close();
    }

}
?>
