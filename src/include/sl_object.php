<?php
// sl_object.php
// SecondLife Object
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

class SecondLifeObject {
    public $key = "";
    public $name = "";
    public $region = "";
    public $position = "";
    public $owner = "";

    public function __construct($server_data) {
        if (!array_key_exists('HTTP_X_SECONDLIFE_OBJECT_NAME', $server_data)) {
            return null;
        }

        $this->key = trim($server_data['HTTP_X_SECONDLIFE_OBJECT_KEY']);
        $this->name = trim($server_data['HTTP_X_SECONDLIFE_OBJECT_NAME']);
        $this->region = trim($server_data['HTTP_X_SECONDLIFE_REGION']);
        $this->position = trim($server_data['HTTP_X_SECONDLIFE_LOCAL_POSITION']);
        $this->owner = trim($server_data['HTTP_X_SECONDLIFE_OWNER_NAME']);
    }
}
?>