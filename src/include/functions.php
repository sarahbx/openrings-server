<?php
// functions.php
// misc functions
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

require_once 'include/config.php';
require_once 'include/database.php';

$CONFIG_FILE = "/config.json";

function slashArray(&$funcInput)
{
  if (is_array($funcInput)) {
    foreach($funcInput as &$value) {
      if (!is_array($value) && !get_magic_quotes_gpc()) {
        $value = addslashes($value);
      }
      else {
        slashArray($value);
      }
    }
    unset($value);
  }
}

function get_database() {
    global $CONFIG_FILE;
    $config = new Config($CONFIG_FILE);
    $credentials = $config->get_credentials();

    return new Database(
        $credentials["address"],
        $credentials["username"],
        $credentials["password"],
        $credentials["dbname"]
    );
}
?>
