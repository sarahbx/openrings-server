<?php
// html_elements.php
// HTML Element variables
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

$html_elements = array();

$html_elements["list_page_head"] = <<<'EOD'
<html>
<head>
    <title>openRings Status Page</title>
</head>
<body bgcolor=black text=white>
<center>
<h1>openRings Status</h1>
EOD;

$html_elements["list_page_footer"] = <<<'EOD'
<br />
Rings send updates every hour.<br />
Old rings are deleted after 3 hours.
</center>
</body>
</html>
EOD;

$html_elements["list_table_head"] = <<<'EOD'
<table border=0>
<tr><th align="center" colspan="3">Region</th></tr>
<tr>
  <td align="center">Ring owner</td>
  <td align="center">Ring Version</td>
  <td align="center">Location</td>
  <td align="center">Last Update</td>
</tr>
EOD;

$html_elements["list_table_region_head"] = <<<'EOD'
<tr><th align="center" colspan="3">%s</th></tr>
EOD;

$html_elements["list_table_row"] = <<<'EOD'
<tr>
    <td align="center">%s</td>
    <td align="center">%s</td>
    <td align="center">%s</td>
    <td align="center">%s</td>
</tr>
EOD;

$html_elements["list_table_footer"] = <<<'EOD'
</table><br />
Regions: %d<br />
Total Number of openRings: %d<br />
<br />
<table border="0">
    <th colspan="2">Versions</th>
EOD;

?>