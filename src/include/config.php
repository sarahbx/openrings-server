<?php
// config.php
// Configuration File
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

class Config {
    private $config = null;

    public function __construct($filename) {
        $config_json = file_get_contents($filename);
        $config_json = utf8_encode($config_json);
        $this->config = json_decode($config_json, true);
        if (!$this->config) {
            error_log("JSON file not loaded");
            return null;
        }
        if (!array_key_exists("sql", $this->config)) {
            error_log("Required 'sql' key not found in config");
            return null;
        }
    }

    public function get_credentials() {
        if ($this->config && array_key_exists("sql", $this->config)) {
            return $this->config["sql"];
        }
        else {
            error_log("credentials do not exist in config file");
            return null;
        }
    }
}
?>
