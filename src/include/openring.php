<?php
// openring.php
// OpenRing Object
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('OPENRINGS')) die();

require_once 'include/sl_object.php';
require_once 'include/functions.php';

class OpenRing extends SecondLifeObject {
    public $version = "";

    public function __construct($server_data) {
        parent::__construct($server_data);
        if ($this->name == "") {
            return null;
        }

        $this->region = trim(substr($this->region, 0, strpos($this->region, "(")-1));
        $this->version = trim(substr($this->name, 10, strlen($this->name)-10));
        $this->name = trim(substr($this->name, 0, 9));

        if ($this->name != "OpenRings") {
            return null;
        }
    }

    public function exists_in_db() {
        $db = get_database();

        $sql_select_fmt = "SELECT * FROM ringlist WHERE ringKey = '%s'";
        $escaped_key = $db->escape($this->key);

        $db_query = sprintf($sql_select_fmt, $escaped_key);
        $db_result = $db->query($db_query);

        if ($db_result) {
            return true;
        }
        else {
            return false;
        }
    }

    public function update_db() {
        $now = time();
        $db = get_database();

        $db_query = "UPDATE ringlist SET ".
                    "age='$now', ".
                    "owner='".$db->escape($this->owner)."', ".
                    "version='".$db->escape($this->version)."', ".
                    "region='".$db->escape($this->region)."', ".
                    "location='".$db->escape($this->position)."' ".
                    "WHERE ringKey='".$db->escape($this->key)."'";

        return $db->query($db_query);
    }

    public function insert_db() {
        $now = time();
        $db = get_database();

        $db_query = "INSERT INTO ringlist (ringKey, age, owner, version, region, location) ".
                    "VALUES ('".$db->escape($this->key).
                    "', '".$now.
                    "', '".$db->escape($this->owner).
                    "', '".$db->escape($this->version).
                    "', '".$db->escape($this->region).
                    "', '".$db->escape($this->position)."')";

        return $db->query($db_query);
    }

    public function purge_db($minutes) {

        $age = time() - ($minutes*60);

        $db_query = sprintf("DELETE FROM ringlist WHERE age < %d", $age);

        $db = get_database();
        $db_result = $db->query($db_query);
    }

    public function list_db() {
        $db = get_database();
        $db_query = "SELECT * FROM ringlist ORDER BY region,owner ASC";
        $db_result = $db->query($db_query) or die("Unable to access ring list");
        return $db_result;
    }
}
?>
