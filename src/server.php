<?php
// server.php
// main server script
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

define('OPENRINGS', 1);

require_once 'include/functions.php';
require_once 'include/openring.php';

function processData(&$openring)
{
  $now = time();
  if ($openring->exists_in_db())
  {
    printf("RING EXISTS, UPDATING...\n");

    $update_result = $openring->update_db();

    if ($update_result) {
      printf("UPDATE OK.\n");
    }
    else {
      printf("ERROR UPDATING.\n");
    }
  }
  else
  {
    printf("NEW RING, INSERTING...\n");

    $insert_result = $openring->insert_db();
    if ($insert_result) {
      printf("INSERT OK.\n");
    }
    else {
      printf("ERROR INSERTING.\n");
    }

  }
}

function purgeOldEntries(&$openring)
{
  printf("PURGING OLD ENTRIES...\n");

  $minutes = (3*60); // three hours old

  $openring->purge_db($minutes);
}

function main() {
    $openring = new OpenRing($_SERVER);
    if ($openring) {
        printf("\nVALID OBJECT\n");
        processData($openring);
        purgeOldEntries($openring);
        printf("COMPLETED.");
    }
    else {
        printf("UNSUPPORTED OBJECT\n");
    }
}

main();
die();
?>