<?php
// list.php
// main server script
//
//    openRings Server System
//    Copyright 2008 (C) Sarah Bennert
//
////////////////////////////////////////////////////////////////////////////////////////////////

define('OPENRINGS', 1);

require_once 'include/functions.php';
require_once 'include/openring.php';
include 'include/html_elements.php';

function printVersions($vArray) {
  krsort($vArray, SORT_STRING);
  foreach ($vArray as $ver => $num) {
    echo "<tr><td>".$ver."</td><td>: ".$num."</td></tr>\n";
  }
}

function displayRings() {
  global $html_elements;

  $db_result = OpenRing::list_db();

  $prevOwner = "";
  $prevRegion = "thisCannotBeBlank";

  printf($html_elements["list_table_head"]);

  $totalRings = 0;
  $totalRegions = 0;
  $totalUsers = 0;
  
  $versionArray = array();
  
  $now = time();
  $timezone = date_default_timezone_get();
  foreach ($db_result as $db_info) {
    $totalRings += 1;
    $ringTime = $db_info['updated']." ".date('T'); // display updated time

    if ($db_info['region'] != $prevRegion) {
      $totalRegions += 1;
      printf($html_elements["list_table_region_head"], $db_info['region']);
      $prevRegion = $db_info['region'];
    }
    
    if ($db_info['owner'] == $prevOwner) {
        printf($html_elements["list_table_row"], "", $db_info['version'], $db_info['location'], $ringTime);
    }
    else {
        printf($html_elements["list_table_row"], $db_info['owner'], $db_info['version'], $db_info['location'], $ringTime);
        $prevOwner = $db_info['owner'];
    }
    if ($db_info['version'] != "") {
      if (isset($versionArray[$db_info['version']])) {
        $versionArray[$db_info['version']] += 1;
      }
      else {
        $versionArray[$db_info['version']] = 1;
      }
    }
  }

  printf($html_elements["list_table_footer"], $totalRegions, $totalRings);

  printVersions($versionArray);

  printf("</table>\n");
}

function main() {
    global $html_elements;

    printf($html_elements["list_page_head"]);

    displayRings();

    printf($html_elements["list_page_footer"]);
}

main();
die();
?>