# OpenRings Server

Very simple server for keeping track of OpenRings in SecondLife.  
Only uses `HTTP_X_SECONDLIFE_*` data from requests to populate the 
database.

## Requirements

* PHP Server
* [MariaDB](https://mariadb.org) or [MySQL](https://www.mysql.com)
** InnoDB engine

## Configuration

Populate [config.example.json](config.example.json) and save it 
to `config.json` outside of the repository. It is expected to be in 
the parent directory to `src`, outside of `public_html`. This location 
can be updated in [include/config.php](include/config.php) via `$CONFIG_FILE` 
if necessary.

**Do NOT commit user/password data to the repository**

## Database

* Database Name: `openrings`
* Table Name: `ringlist`

A database schema is provided in [schema.sql](schema.sql).  

### Creating the database:

Log into the database server:
```bash
mysql -u root -p
```

On the MariaDB or MySQL command line:
```
CREATE DATABASE `openrings` DEFAULT CHARSET = utf8 DEFAULT COLLATE = utf8_unicode_ci;
CREATE USER '<db_ring_user>'@'localhost' IDENTIFIED BY '<password>';
GRANT SELECT, INSERT, UPDATE, DELETE ON openrings.ringlist TO '<db_ring_user>'@'localhost';
```

Back in the shell:
```bash
mysql -u root -p openrings < schema.sql
```
